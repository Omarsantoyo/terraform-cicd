#testing CICD
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "resource-devops"
    storage_account_name = "omardoustorage"
    container_name       = "omardoucontainer"
    key                  = "terraform.tfstate"
  }
}

# Configuring providers
provider "azurerm" {
  features {}
}

# Create virtual network
resource "azurerm_virtual_network" "network" {
  name                = "myVN"
  resource_group_name = "resource-devops"
  location            = "East US"
  address_space       = ["10.0.0.0/16"]
  
}

output "example_id" {
  value = azurerm_virtual_network.network.id
}

